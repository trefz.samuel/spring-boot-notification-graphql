package com.notitifcationservice.demo;

import com.notitifcationservice.demo.controller.Category;
import com.notitifcationservice.demo.controller.CategoryRepository;
import com.notitifcationservice.demo.controller.Notification;
import com.notitifcationservice.demo.controller.NotificationRepository;
import jdk.jfr.Category;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.management.Notification;
import java.util.List;

@SpringBootApplication
public class NotificationserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotificationserviceApplication.class, args);
	}

	@Bean
	ApplicationRunner applicationRunner(CategoryRepository categoryRepository, NotificationRepository notificationRepository) {
		return args -> {
			Category usertask = categoryRepository.save(new Category(null, "usertask"));
			notificationRepository.saveAll(List.of(
					new Notification(1, "usertask", "You have a new Usertask to solve"),
					new Notification(1, "usertask", "You have a new Usertask to solve")
			));

		};
	}

}
