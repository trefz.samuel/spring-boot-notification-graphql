package com.notitifcationservice.demo.controller;

import javax.persistence.*;

@Entity
public class Notification {
    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String body;
    private String actionLink;
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;



    public Notification(Object id, String usertask, String body) {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getActionLink() {
        return actionLink;
    }

    public void setActionLink(String actionLink) {
        this.actionLink = actionLink;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
